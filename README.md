# react-tk-resetpassword

> reusable resetpassword package for tk

[![NPM](https://img.shields.io/npm/v/react-tk-resetpassword.svg)](https://www.npmjs.com/package/react-tk-resetpassword) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-tk-resetpassword
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-tk-resetpassword'
import 'react-tk-resetpassword/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [fernando_molinari](https://github.com/fernando_molinari)
