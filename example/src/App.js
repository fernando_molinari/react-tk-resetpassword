import React from "react";
import ResetPasswordContent from "react-tk-resetpassword";
import "react-tk-resetpassword/dist/index.css";

const App = () => {
  const [rpBtnEnabled, setRpBtnEnabled] = React.useState(false);
  const onResetPasswordClick = (payload) => {
    setRpBtnEnabled(!rpBtnEnabled);
  };

  return (
    <div className="App">
      <ResetPasswordContent
        onResetPasswordClick={onResetPasswordClick}
        schoolName=""
        resetPasswordBtnEnabled={rpBtnEnabled}
        headerText="Thalamus Knowledge"
      />
    </div>
  );
};

export default App;
