/*
 * Copyright 2021 by - - Fernando Molinari.
 * All rights reserved.
 */

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import clsx from 'clsx';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const ResetPasswordContent = (props) => {
  const { schoolName, resetPasswordBtnEnabled, headerText, t } = props;

  const useStyles = makeStyles((theme) => ({
    paper: {
      margin: headerText ? theme.spacing(2, 4) : theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    btnWrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
      width: '35%',
    },
    margin: {
      margin: theme.spacing(1),
    },
    textField: {
      width: '100%',
    },
    paragraph: {
      fontSize: '0.8rem',
      marginTop: '20px',
    },
    headerText: {
      marginTop: '0',
      marginBottom: '10px',
    },
  }));

  const classes = useStyles();

  let sName = '';

  if (schoolName) {
    sName = `/${schoolName}`;
  }

  const [btnDisabled, setBtnDisabled] = useState(false);
  const [submitLoading, setSubmitLoading] = useState(false);

  const [password, setPassword] = useState(null);
  const [isPasswordHelperText, setIsPasswordHelperText] = useState('');

  const [passwordAgain, setPasswordAgain] = useState(null);
  const [isPasswordAgainHelperText, setIsPasswordAgainHelperText] = useState(
    '',
  );

  const onHandlePassword = (e) => {
    setIsPasswordHelperText('');
    setPassword(e.target.value);
  };

  const onHandlePasswordAgain = (e) => {
    setIsPasswordAgainHelperText('');
    setPasswordAgain(e.target.value);
  };

  // show/hide password
  const [values, setValues] = useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const [valuesAgain, setValuesAgain] = useState({
    amount: '',
    password: '',
    weight: '',
    weightRange: '',
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleClickShowPasswordAgain = () => {
    setValuesAgain({ ...valuesAgain, showPassword: !valuesAgain.showPassword });
  };

  const handleMouseDownPasswordAgain = (event) => {
    event.preventDefault();
  };
  // show/hide password

  const onResetPasswordClick = (event) => {
    event.preventDefault();

    if (!password) {
      setIsPasswordHelperText(
        t ? t('plsNewPassword') : 'Please type in your new password',
      );
      return false;
    }

    if (!passwordAgain) {
      setIsPasswordAgainHelperText(
        t ? t('plsNewPasswordAgain') : 'Please type in your new password again',
      );
      return false;
    }

    if (password.toLowerCase() !== passwordAgain.toLowerCase()) {
      setIsPasswordAgainHelperText(
        t ? t('passwordsDontMatch') : 'Passwords do not match',
      );
      return false;
    }

    const payload = {
      password,
    };

    props.onResetPasswordClick(payload);

    setBtnDisabled(true);
    setSubmitLoading(true);
  };

  useEffect(() => {
    setBtnDisabled(false);
    setSubmitLoading(false);
  }, [resetPasswordBtnEnabled]);

  return (
    <div className={classes.paper}>
      {headerText ? (
        <Typography component='h1' variant='h6' className={classes.headerText}>
          {headerText}
        </Typography>
      ) : null}
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component='h1' variant='h6'>
        {t ? t('resetPassword') : 'Reset Password'}
      </Typography>
      <Typography component='h6' className={classes.paragraph}>
        {t ? t('plsNewPassword') : 'Please type in your new password'}
      </Typography>
      <form className={classes.form} noValidate>
        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant='outlined'
        >
          <InputLabel htmlFor='password'>New Password</InputLabel>
          <OutlinedInput
            required
            fullWidth
            id='password'
            type={values.showPassword ? 'text' : 'password'}
            name='password'
            autoComplete='password'
            onChange={onHandlePassword}
            endAdornment={
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge='end'
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            aria-describedby='outlined-password-helper-text'
            inputProps={{
              'aria-label': 'password',
            }}
            labelWidth={110}
            autoFocus
          />
          <FormHelperText id='outlined-password-helper-text'>
            {isPasswordHelperText}
          </FormHelperText>
        </FormControl>

        <FormControl
          className={clsx(classes.margin, classes.textField)}
          variant='outlined'
        >
          <InputLabel htmlFor='passwordAgain'>
            {t ? t('newPasswordAgain') : 'Type New Password Again'}
          </InputLabel>
          <OutlinedInput
            required
            fullWidth
            name='passwordAgain'
            type={valuesAgain.showPassword ? 'text' : 'password'}
            id='passwordAgain'
            autoComplete='password again'
            onChange={onHandlePasswordAgain}
            endAdornment={
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password again visibility'
                  onClick={handleClickShowPasswordAgain}
                  onMouseDown={handleMouseDownPasswordAgain}
                  edge='end'
                >
                  {valuesAgain.showPassword ? (
                    <Visibility />
                  ) : (
                    <VisibilityOff />
                  )}
                </IconButton>
              </InputAdornment>
            }
            aria-describedby='outlined-passwordagain-helper-text'
            inputProps={{
              'aria-label': 'passwordagain',
            }}
            labelWidth={200}
          />
          <FormHelperText id='outlined-passwordagain-helper-text'>
            {isPasswordAgainHelperText}
          </FormHelperText>
        </FormControl>

        <Grid className={classes.btnWrapper}>
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={onResetPasswordClick}
            disabled={btnDisabled}
            startIcon={
              submitLoading ? (
                <CircularProgress size='1.0rem' color='primary' />
              ) : null
            }
          >
            {t ? t('submit') : 'Submit'}
          </Button>
        </Grid>
        <Grid container>
          <Grid item xs>
            <Link href={`${sName}/signin`} variant='body2'>
              {t ? t('backSignin') : 'Back to Sign In'}
            </Link>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default ResetPasswordContent;

ResetPasswordContent.propTypes = {
  schoolName: PropTypes.string,
  resetPasswordBtnEnabled: PropTypes.bool,
  onResetPasswordClick: PropTypes.func,
  t: PropTypes.func,
};
